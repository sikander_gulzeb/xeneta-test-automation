package com.cdcw.tests;
import java.util.List;
import java.util.concurrent.TimeUnit;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.cdcw.commonfunctionality.Constants;
import com.cdcw.commonfunctionality.LoginManager;
import com.cdcw.commonfunctionality.ResultManager;
import com.cdcw.commonfunctionality.WaitManager;
import com.cdcw.commonfunctionality.WebDriverManager;


public class DayPassTestCase {
	private Constants constantsObject = new Constants();
	private WaitManager waitManager = new WaitManager ();
	private LoginManager loginManager = new LoginManager();
	private WebDriverManager webDriverManager = new WebDriverManager();
	private ResultManager resultManager = new ResultManager();
	private WebDriver driver;
	private WebDriverWait waiter;
	private boolean isPassed = false;
	
	public void orderDayPassForCustomer() throws Exception {
		WebElement dayPassTab;
		WebElement moveOnButton;
		WebElement orderButton;
		waitManager.waitForPageToLoadCompletely(constantsObject.getDayPassTabXpath(),driver);
		dayPassTab = driver.findElement(By.xpath(constantsObject.getDayPassTabXpath()));
		dayPassTab.click();
		moveOnButton = driver.findElement(By.xpath(constantsObject.getMoveOnButtonXpath()));
		moveOnButton.click();
		waitManager.waitForPageToLoadCompletely(constantsObject.getOrderButtonXpath(),driver);
		orderButton = driver.findElement(By.xpath(constantsObject.getOrderButtonXpath()));
		orderButton.click();
	}
	@SuppressWarnings("deprecation")
	public void checkIfRecieptGenerated() throws Exception{
		if(!errorOnDayPassSubmission()){
			waitManager.waitForPageToLoadCompletely(constantsObject.getDayPassRecieptHeaderXpath(),driver);
			if (driver.findElements( By.xpath(constantsObject.getDayPassRecieptHeaderXpath())).size() != 0)
				isPassed = true;
			else
				isPassed = false;
		}
		else
			isPassed = false;
			
	}
	
	public boolean errorOnDayPassSubmission() throws Exception{
		Thread.sleep(constantsObject.getSyncTime());
		WebElement serverError = driver.findElement(By.id(constantsObject.getServerErrorId()));
		
		if(serverError.isDisplayed()){
			System.err.println("Server Error on ordering day pass");
			return true;
		}
		else
			return false;
	}
	
	public void setupTest() throws Exception{
		driver = webDriverManager.getDriver();
		waiter = webDriverManager.getWaiter();
	}
	
	@Test
	public void test_dayPassTest1(){
		try {
			webDriverManager.setUp();
			setupTest();
			loginManager.login(driver, waiter);
			driver.get(constantsObject.getOffersPageUrl());
			orderDayPassForCustomer();
			checkIfRecieptGenerated();
			resultManager.setResult(isPassed);	
		}catch (Exception e){
			System.out.println(e.getMessage().toString());
		}
		finally{
			loginManager.closePage(driver);
		}
	}
	@Test
	public void test_dayPassTest2(){
		Assert.assertTrue(true);
	}
}
