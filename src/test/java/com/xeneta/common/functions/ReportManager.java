package com.xeneta.common.functions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReportManager {
	private static final String resultPlaceholder = "<!-- INSERT_RESULTS -->";
	private static final String templatePath = "src\\main\\resources\\template.html";
	static String reportIn;
	private static ReportManager reportManager = null;
	private Constants constantsObject = new Constants();

	public static ReportManager getReportManager() throws IOException {
		if (reportManager == null) {
			reportManager = new ReportManager();
			reportIn = new String(Files.readAllBytes(Paths.get(templatePath)));
		}
		return reportManager;
	}

	public void writeReport(String testName, String status) {
		try {
			addRow(testName, status);
			String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			String reportPath = System.getProperty("user.home") + "/Desktop" + "\\report_" + currentDate + " .html";
			Files.write(Paths.get(reportPath), reportIn.getBytes(), StandardOpenOption.CREATE);
		} catch (Exception e) {
			System.out.println("Error when writing report file:\n" + e.toString());
		}
	}

	private void addRow(String testName, String status) {
		if (status.equals("passed"))
			reportIn = reportIn.replaceFirst(resultPlaceholder, "<tr bgcolor=\"#008000\" align=\"center\"><td><font color=\"#fff\">"
					+ testName + "</font></td><td><font color=\"#fff\">" + status + "</font></td></tr>\n" + resultPlaceholder);

		else
			reportIn = reportIn.replaceFirst(resultPlaceholder, "<tr bgcolor=\"#FF0000\" align=\"center\"><td><font color=\"#fff\">"
					+ testName + "</font></td><td><font color=\"#fff\">" + status + "</font></td></tr>\n" + resultPlaceholder);

	}
}
