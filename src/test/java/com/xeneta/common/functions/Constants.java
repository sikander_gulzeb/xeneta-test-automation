package com.xeneta.common.functions;

public class Constants {
	private String userName = "SE19059060";
	private String password = "SE19059060";
	private String url = "http://stage.canaldigital.se/login?ReturnUrl=%2fmina-sidor%2foversikt%2f";
	private String chromeDriverPath = "src\\main\\resources\\chromedriver.exe";
	private String offersPageUrl = "http://stage.canaldigital.se/mina-sidor/erbjudanden/";
	private int maxTryLimit = 50;
	private int syncTime = 2000;

	private String xenetaDemoPageUrl = "https://www.xeneta.com/test-request-xeneta-demo";
	private String mediaResourcesPageUrl = "https://www.xeneta.com/media-resources";

	// ids
	private String firstNameElementName = "firstname";
	private String lastNameElementName = "lastname";
	private String emailElementName = "email";
	private String jobElementName = "jobtitle";
	private String companyElementName = "company";
	private String phoneNumberElementName = "direct_phone__c";
	private String prospectElementName = "type_of_prospect";
	private String shippedElementName = "teu_shipped_anually";
	private String responsibleElementName = "persona_2018";
	private String demoPlacedElementId = "hs_cos_wrapper_widget_1511361239949_title";
	private String newsletterEmailName = "email";
	private String newsletterToggleName = "confirm_opt_in";
	private String newsletterSuccessMessageId = "hs_form_target_module_1490890020720863_Footer_form";
	

	// xpaths
	private String submitButtonXpath = "//*[@id=\"hsForm_f35065d3-213f-413c-9426-48fd85c9fb27_1752\"]/div[13]/div[2]/input";
	private String youtubeTitleXpath = "//*[@id=\"title\"]/a";
	private String dutchLanguageXpath = "//*[@id=\"hs_cos_wrapper_module_1490890027164867_language_switcher\"]/div/div/ul/li[2]/a";
	private String dutchHeaderXpath = "//*[@id=\"hs_cos_wrapper_module_1490890027164867\"]/header/div[2]/h1";
	private String successXpath = "//*[@id=\"hs_form_target_module_1490890020720863_Footer_form\"]";
	
	
	public static String DEMO_VIDEO_TEST_NAME = "Verify that the Demo vedio exist on Demo page";
	public static String LANGUAGE_CHANGE_TEST_NAME = "Verify that the language of Site changes From  English to Dutch";
	public static String NEWSLETTER_TEST_NAME = "Verify that the Subscription to the Newsletter successfully";
	public static String REQUEST_DEMO_TEST_NAME = "Verify that the request for demo is submitted successfully";
	public static String TRUSTED_COMPANY_TEST_NAME = "Verify that all the logo images of the companies Exist";
	
	
	
	
	private String[] trustedCompanyLogosXpath = {
			"//*[@id=\"hs_cos_wrapper_widget_1515082237069\"]/div/a/ul/li[2]/img",
			"//*[@id=\"hs_cos_wrapper_widget_1515082237069\"]/div/a/ul/li[5]/img",
			"//*[@id=\"hs_cos_wrapper_widget_1515082237069\"]/div/a/ul/li[4]/img",
			"//*[@id=\"hs_cos_wrapper_widget_1515082237069\"]/div/a/ul/li[1]/img",
			"//*[@id=\"hs_cos_wrapper_widget_1515082237069\"]/div/a/ul/li[6]/img" };
	
	public String getXenetaDemoPageDemoPageUrl() {
		return xenetaDemoPageUrl;
	}

	public String getChromeDriverPath() {
		return chromeDriverPath;
	}

	public String getOffersPageUrl() {
		return offersPageUrl;
	}

	public int getMaxTryLimit() {
		return maxTryLimit;
	}

	public int getSyncTime() {
		return syncTime;
	}

	public String getSubmitButtonXpath() {
		return submitButtonXpath;
	}

	public String getFirstNameElementId() {
		return firstNameElementName;
	}

	public String getLastNameElementId() {
		return lastNameElementName;
	}

	public String getEmailElementId() {
		return emailElementName;
	}

	public String getJobElementId() {
		return jobElementName;
	}

	public String getCompanyElementId() {
		return companyElementName;
	}

	public String getPhoneNumberElementId() {
		return phoneNumberElementName;
	}

	public String getProspectElementId() {
		return prospectElementName;
	}

	public String getShippedElementId() {
		return shippedElementName;
	}

	public String getResponsibleElementId() {
		return responsibleElementName;
	}

	public String getDemoPlacedElementId() {
		return demoPlacedElementId;
	}

	public String getYoutubeTitleXpath() {
		return youtubeTitleXpath;
	}

	public String[] getTrustedCompanyLogosXpath() {
		return trustedCompanyLogosXpath;
	}

	public String getMediaResourcesPageUrl() {
		return mediaResourcesPageUrl;
	}

	public String getDutchLanguageXpath() {
		return dutchLanguageXpath;
	}

	public String getDutchHeaderXpath() {
		return dutchHeaderXpath;
	}

	public String getNewsletterEmailId() {
		return newsletterEmailName;
	}

	public String getNewsletterToggleId() {
		return newsletterToggleName;
	}

	public String getNewsletterSuccessMessageId() {
		return newsletterSuccessMessageId;
	}

	public String getSuccessXpath() {
		return successXpath;
	}
}
