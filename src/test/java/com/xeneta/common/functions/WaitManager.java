package com.xeneta.common.functions;
import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WaitManager {
	private Constants constantsObject = new Constants();
	private WebElement element;
	WebDriverWait wait;
	
	public void waitForPageToLoadCompletely(String objectXpath,WebDriver driver) throws Exception{
		wait = new WebDriverWait(driver, 30);
		int tries = 0;
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(objectXpath)));
		Thread.sleep(constantsObject.getSyncTime());
		while (tries <= constantsObject.getMaxTryLimit()){
			System.out.println(tries + "for " + objectXpath);
			Thread.sleep(constantsObject.getSyncTime());
			tries++;
			if(driver.findElements(By.xpath(objectXpath)).size() > 0 )
				break;
		}
		if(tries > constantsObject.getMaxTryLimit()){
			System.out.println("Unable to load object with xpath: " + objectXpath);
			Assert.assertTrue(false);
		}
		driver.findElement(By.tagName("Body")).sendKeys(Keys.ESCAPE);
	}
}
