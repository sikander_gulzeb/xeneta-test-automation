package com.xeneta.common.functions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WebDriverManager {
	private WebDriver driver;
	private WebDriverWait waiter;
	WebDriverWait wait;
	private Constants constantsObject = new Constants();
	
	public void setUp() throws Exception{
		System.setProperty("webdriver.chrome.driver", constantsObject.getChromeDriverPath());
		driver = new ChromeDriver();
		driver.get("http://www.google.com");
		waiter = new WebDriverWait(driver, 5000);
	}

	public WebDriver getDriver() throws Exception {
		if (this.driver != null)
			return this.driver;
		else {
			setUp();
			return this.driver;
		}	
	}

	public WebDriverWait getWaiter() {
		return waiter;
	}
	
}
