package com.xeneta.common.functions;

import junit.framework.Assert;

public class ResultManager {

	public void setResult(boolean isPassed, String testName) throws Exception{
		if (isPassed) {
            ReportManager.getReportManager().writeReport(testName, "passed");
            Assert.assertTrue(true);
        } else {
            ReportManager.getReportManager().writeReport(testName, "failed");
            Assert.fail("Test failed");
        }
	}
}
