package com.xenetea.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.xeneta.common.functions.Constants;
import com.xeneta.common.functions.ResultManager;
import com.xeneta.common.functions.WaitManager;
import com.xeneta.common.functions.WebDriverManager;

public class NewsLetterTestCase {
	
	private Constants constantsObject = new Constants();
	private WebDriverManager webDriverManager = new WebDriverManager();
	private ResultManager resultManager = new ResultManager();
	private WebDriver driver;
	private WebDriverWait waiter;
	private WaitManager waitManager = new WaitManager ();
	
	public void setupTest() throws Exception{
		driver = webDriverManager.getDriver();
		waiter = webDriverManager.getWaiter();
	}
	
	public void signUpForNewsLetter() throws Exception {
		WebElement email, submit, toggle;
		
		email = driver.findElement(By.name(constantsObject.getNewsletterEmailId()));
		toggle = driver.findElement(By.name(constantsObject.getNewsletterToggleId()));
		
		email.sendKeys("sikander.gulzeb@telenor.com");
		toggle.click();
		
		email.sendKeys(Keys.RETURN);
		
	}

	@Test
	public void newsLetterTest() {
		try {
			webDriverManager.setUp();
			setupTest();
			driver.get(constantsObject.getMediaResourcesPageUrl());
			signUpForNewsLetter();
			boolean result = checkIfSuccess();
			resultManager.setResult(result, Constants.NEWSLETTER_TEST_NAME);
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false);
		} finally {
			//closePage
			driver.close();
		}
	}

	private boolean checkIfSuccess() throws Exception {
		WebElement successMessage;
		waitManager.waitForPageToLoadCompletely(constantsObject.getSuccessXpath(), driver);
		successMessage = driver.findElement(By.id(constantsObject.getNewsletterSuccessMessageId()));
		if(successMessage.getText().equalsIgnoreCase("Success! Thank you for signing up to the Xeneta newsletter!"))
				return true;
		return false;
	}
}
