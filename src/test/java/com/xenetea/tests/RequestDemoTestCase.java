package com.xenetea.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.xeneta.common.functions.Constants;
import com.xeneta.common.functions.ResultManager;
import com.xeneta.common.functions.WebDriverManager;

public class RequestDemoTestCase {
	private Constants constantsObject = new Constants();
	private WebDriverManager webDriverManager = new WebDriverManager();
	private ResultManager resultManager = new ResultManager();
	private WebDriver driver;
	private WebDriverWait waiter;
	
	public void setupTest() throws Exception{
		driver = webDriverManager.getDriver();
		waiter = webDriverManager.getWaiter();
	}
	
	public void requestDemo() throws Exception {
		WebElement firstName,lastName,jobTitle,companyName, email,submit,phoneNumber;
		
		//waitManager.waitForPageToLoadCompletely(constantsObject.getSubmitButtonXpath(),driver);
		firstName = driver.findElement(By.name(constantsObject.getFirstNameElementId()));
		lastName = driver.findElement(By.name(constantsObject.getLastNameElementId()));
		jobTitle = driver.findElement(By.name(constantsObject.getJobElementId()));
		companyName = driver.findElement(By.name(constantsObject.getCompanyElementId()));
		email = driver.findElement(By.name(constantsObject.getEmailElementId()));
		submit = driver.findElement(By.className("hs-button"));
		phoneNumber = driver.findElement(By.name(constantsObject.getPhoneNumberElementId()));
		
		Select prospect = new Select(driver.findElement(By.name(constantsObject.getProspectElementId())));
		Select shipped = new Select(driver.findElement(By.name(constantsObject.getShippedElementId())));
		Select responsible = new Select(driver.findElement(By.name(constantsObject.getResponsibleElementId())));
		prospect.selectByVisibleText("Consultant");
		shipped.selectByVisibleText("Less than 500");
		responsible.selectByVisibleText("C-Level Strategic Decisions");
		
		firstName.sendKeys("Sikander");
		lastName.sendKeys("Gulzeb");
		jobTitle.sendKeys("Automation Specialist");
		companyName.sendKeys("Xeneta");
		email.sendKeys("sikander.gulzeb@telenor.com");
		phoneNumber.sendKeys("12345678");
		submit.click();
	}

	@Test
	public void requestDemoTestCase() {
		try {
			webDriverManager.setUp();
			setupTest();
			driver.get(constantsObject.getXenetaDemoPageDemoPageUrl());
			requestDemo();
			boolean result = checkIfDemoPlaced();
			resultManager.setResult(result, Constants.REQUEST_DEMO_TEST_NAME);
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false);
		} finally {
			//closePage
			driver.close();
		}
	}

	private boolean checkIfDemoPlaced() {
		WebElement header;
		header = driver.findElement(By.id(constantsObject.getDemoPlacedElementId()));
		if(header.isDisplayed())
				return true;
		return false;
	}

}
