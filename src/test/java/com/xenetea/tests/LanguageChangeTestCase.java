package com.xenetea.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.xeneta.common.functions.Constants;
import com.xeneta.common.functions.ResultManager;
import com.xeneta.common.functions.WaitManager;
import com.xeneta.common.functions.WebDriverManager;

public class LanguageChangeTestCase {
	private Constants constantsObject = new Constants();
	private WebDriverManager webDriverManager = new WebDriverManager();
	private ResultManager resultManager = new ResultManager();
	private WebDriver driver;
	private WebDriverWait waiter;
	private WaitManager waitManager = new WaitManager ();
	
	public void setupTest() throws Exception{
		driver = webDriverManager.getDriver();
		waiter = webDriverManager.getWaiter();
	}
	
	@Test
	public void changeLanguageTest() {
		try {
			webDriverManager.setUp();
			setupTest();
			driver.get(constantsObject.getMediaResourcesPageUrl());
			boolean result = changeLanguage();
			resultManager.setResult(result, Constants.LANGUAGE_CHANGE_TEST_NAME);
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false);
		} finally {
			//closePage
			driver.close();
		}
	}

	private boolean changeLanguage() throws Exception {
		waitManager.waitForPageToLoadCompletely(constantsObject.getDutchLanguageXpath(),driver);
		WebElement language = driver.findElement(By.xpath(constantsObject.getDutchLanguageXpath()));
		language.click();
		
		waitManager.waitForPageToLoadCompletely(constantsObject.getDutchHeaderXpath(),driver);
		WebElement header = driver.findElement(By.xpath(constantsObject.getDutchHeaderXpath()));
		
		if(header.getText().equalsIgnoreCase("Pressematerialien"))
			return true;
		return false;
	}
}
