package com.xenetea.tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.xeneta.common.functions.Constants;
import com.xeneta.common.functions.ResultManager;
import com.xeneta.common.functions.WaitManager;
import com.xeneta.common.functions.WebDriverManager;

public class DemoVideoTestCase {
	private Constants constantsObject = new Constants();
	private WebDriverManager webDriverManager = new WebDriverManager();
	private ResultManager resultManager = new ResultManager();
	private WebDriver driver;
	private WebDriverWait waiter;

	private WaitManager waitManager = new WaitManager ();
	
	public void setupTest() throws Exception{
		driver = webDriverManager.getDriver();
		waiter = webDriverManager.getWaiter();
	}
	
	@Test
	public void checkYoutubeVideoLinkTest() {
		try {
			webDriverManager.setUp();
			setupTest();
			driver.get(constantsObject.getXenetaDemoPageDemoPageUrl());
			boolean result = checkVideo();
			resultManager.setResult(result, Constants.DEMO_VIDEO_TEST_NAME);
		} catch (Exception e) {
			System.out.println(e.getMessage().toString());
			Assert.assertTrue(false);
		} finally {
			//closePage
			driver.close();
		}
	}

	private boolean checkVideo() throws Exception {
		driver.findElement(By.cssSelector("a[href*='youtube']")).click();
		waitManager.waitForPageToLoadCompletely(constantsObject.getYoutubeTitleXpath(),driver);
		WebElement youtubeTitle = driver.findElement(By.xpath(constantsObject.getYoutubeTitleXpath()));
		if(youtubeTitle.getText().equalsIgnoreCase("Xeneta - Changing Shipping & Global Logistics"))
			return true;
		
		return false;
	}
}
