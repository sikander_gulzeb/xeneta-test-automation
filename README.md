# xeneta-test-automation

Steps to Run Selenium (JAVA) Gradle Project:

1.	Download and Install Gradle version:
2.	Download the project and unzip
3.	Go into Xeneta folder
4.	Open cmd in this location
5.	Run the following command each time you want to run the tests
	gradle clean
	gradle test
6.	Tests will start to run one after the other
7.	Once the tests run and build is successful. Navigate to Desktop to find the HTML report by the name of report-<date>.html.
8.	Open thi report in any web browser.
9.	All passed test cases are highlighted in green and similarly, the ones that fail will be in red.

Steps To open the source code in Eclipse:

1. Download and Open Eclipse.
2. Run the following command in cmd in  location ( where project is saved)  to convert the gradle project to an eclipse importable format.
		gradle eclipse
3. In Eclipse, Import the project.
4. Go to source folder;  src/test/java.
5. Find the following test classes:
		i.	DemoVideoTestCase
		ii.	LanguageChangeTestCase
		iii.NewsletterTestCase
		iv.	RequestDemoTestCase
	    v.	TrustedCompanyLogoTestCase
6. In each of these classes, right click and run as -> JUNIT test, this will run that particular test class
7. If from eclipse, you want to run all tests:
		i.	Go to XenetaTestManager.java file
		ii.	Uncomment the line numbers 7 and 8.
		iii.	Right click in this class
		iv.	Run as -> Junit test.
